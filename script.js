const container = document.getElementById('container');
const list = document.createElement('ul');
const form = document.createElement('form');
const inp = document.createElement('input');
const btn = document.createElement('button');
const time = document.createElement('div');

list.classList.add('list');
inp.type = 'number';
inp.id = 'inp';
btn.classList.add('btn');
btn.textContent = "Начать игру";


form.append(btn);
form.append(inp);
container.append(form);
container.append(list);


let arr = [];

let res

btn.addEventListener('click', () => {
    let num = document.getElementById('inp').value;
        if (num === "") {
            return num
        } 
        else {
            const limit = Math.round(num * num / 2);

            for (k = 1; k <= limit; k++) {
                arr.push(k);
                arr.push(k);
            }

                const shuffle = (array) => {
                    let m = array.length, t, i;

                    while (m) {
                        i = Math.floor(Math.random() * m--);
                        t = array[m];
                        array[m] = array[i];
                        array[i] = t;
                    }
                    return array;
                }

                res = shuffle(arr)

                createCard(num * num)
                btn.setAttribute('disabled', 'disabled')
                inp.setAttribute('disabled', 'disabled')
                return res
        }
 
})

let i = 0;
let ex = 0;
let j = 0;

function createCard (num) {
    const card = document.createElement('li');
    
    card.classList.add('card');
    list.append(card);
    card.innerHTML = res[i]
    i++

    card.addEventListener('click', () => {
        if (card.classList.contains('open') === false && card.classList.contains('success') === false) {
            j++
            card.id = j
            card.classList.add('open')
        }
        if (j % 2 === 0) {
            const now = document.getElementById(j);
            const prev = document.getElementById(j - 1);
            if (prev.innerHTML != now.innerHTML) {
                setTimeout(() => {
                    now.classList.remove('open')
                    prev.classList.remove('open')
                }, 500)
            } 
            else {
                setTimeout(() => {
                    now.classList.add('success')
                    prev.classList.add('success')
                }, 500)
                ex += 2
            }
    
            if (ex >= num) {
                ex = 0
                const btnReset = document.createElement('button');
                btnReset.classList.add('btn')
                btnReset.textContent = 'Сыграть еще раз'
                container.append(btnReset)

                btnReset.addEventListener('click', function () {
                    window.location.reload();
                })
            }
        }
    })
    
    if (i >= num) {
        return card
    }

    return createCard(num)
}